##### 一、新建用户，密码设置 

1、sudo useradd zeng

​      sudo passwd zeng

连接之后，只有$，缺少家目录

解决方法：先删除zeng用户 然后再创建用户

sudo userdel zeng

sudo useradd -m -s/bin/bash zeng

2、另外一个高级命令，自动创建家目录和设置bash为新用户的shell

sudo adduser zeng

##### 二、登录新用户

1、新建一个连接

2、直接用su命令切换到新用户   su zeng

##### 三、在自己的家目录，依次建立文件夹

mkdir 国家/省份/.....  -p

-p 的效果：有则忽略无则创建，而不报错

##### 四、在自己的文件夹下创建一个存放个人信息的文件夹，并写内容

touch 曾.txt

vim 曾.txt

1、vim的三种模式

（1）普通模式：不能编辑，默认的模式，按i 就可以进入到编辑模式

（2）编辑模式：可以进行文字的相关编辑，按ESC退回到普通模式

（3）命令行模式：通过命令执行保存（write），退出（quit），强制执行（！）,按ESC后，回到普通模式，然后再英文输入法情况下，shift+：；进入命令行模式

##### 五、将这个国家文件夹打包压缩，然后再解压到中国这个文件夹中

tar -cvzf 国家.gz 国家

mkdir 中国 && tar -xzvf 国家.gz -c

##### 六、用命令下载文件到本地

scp

1、下载：scp 用户名@服务器IP：/要下载的具体文件路径 本地电脑

2、上传：scp 本地电脑文件的路径 用户名@服务器IP：/要上传到的文件目录

scp zeng@12.37.167.126 :/home/zeng/国家.gz d:/

上传补充的内容

scp d:/补充.jpg zeng@121.37.167.126 :home/zeng/文件名

sftp

用户名@服务器IP    get下载    put上传   +文件名

