##### grep

主要用于查找匹配特定模式的行 只能查看

##### sed

主要用于对文本的增删改查

##### awk

主要用于处理结构化的数据



##### awk语句块

1、开始语句块（BEGIN模式） 用来做初始化设置

FS 设置源文件输入的分隔符

OFS 输出的分隔符

/t 空格

$NF 最后一列

2、通用语句块   用来依次处理文件内容

三元运算：条件运算符 ？：

'{isok = $2 >=60 ? "及格":"不及格";print isok}'

##### $1+0是将字符串变成数字的方式，＋0即可

3、结束语句块

例如计算总和和平均

##### 作业

```
1. 只显示/etc/passwd的账户

cat etc/passwd | awk -F: '{print $1}' 
//要先进入到根目录cd /

2. 只显示/etc/passwd的账户和对应的shell，并在第一行上添加列名用户制表符shell，最后一行添加----------------

awk -F: 'BEGIN {print "用户制表符\t shell"} {print $1,$7} END {
print "-----------------"
}' etc/passwd

3. 搜索/etc/passwd有关键字root的所有行

 awk -F: '/root/' etc/passwd
 
4. 统计/etc/passwd文件中，每行的行号，每列的列数，对应的完整行内容以制表符分隔

awk -F: '{print NR "\t" NF "\t"$0}' etc/passwd

5. 输出/etc/passwd文件中以nologin结尾的行

awk -F: '/nologin$/' etc/passwd

6. 输出/etc/passwd文件中uid字段小于100的行

awk -F: '$3 < 100' etc/passwd

7. /etc/passwd文件中gid字段大于200的，输出该行第一、第四字段，第一，第四字段并以制表符分隔

awk -F: '$4 > 200 {print $1 "\t" $4}' etc/passwd
8. 输出/etc/passwd文件中uid字段大于等于100的行 

awk -F: '$3 >= 100' etc/passwd
```

